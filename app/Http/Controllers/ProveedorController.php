<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProveedorController extends Controller
{
    
    public function index()
    {
        $proveedor = Proveedor::all();
        return response()->json($proveedor);
    }

    
    public function store(Request $request)
    {
        $rules = ['proveedor' => 'required|string|min:1|max:100'];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $proveedor = new Proveedor($request->input());
        $proveedor->save();
        return response()->json([
            'status' => true,
            'message' => 'Proveedor creado'
        ],200);
    }

    
    public function show(Proveedor $proveedor)
    {
        return response()->json([
            'status' => true,
            'data' => $proveedor
        ]);
    }

    
    public function update(Request $request, Proveedor $proveedor)
    {
        $rules = ['proveedor' => 'required|string|min:1|max:100'];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $proveedor->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Proveedor actualizado'
        ],200);
    }

    
    public function destroy(Proveedor $proveedor)
    {
        $proveedor->delete();
        return response()->json([
            'status' => true,
            'message' => 'Proveedor eliminada'
        ],200);
    }
}
