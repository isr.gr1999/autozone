<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    
    public function index()
    {
        $categoria = Categoria::all();
        return response()->json($categoria);
    }

    
    public function store(Request $request)
    {
        $rules = ['categoria' => 'required|string|min:1|max:100'];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $categoria = new Categoria($request->input());
        $categoria->save();
        return response()->json([
            'status' => true,
            'message' => 'Categoria creada'
        ],200);
    }

    
    public function show(Categoria $categoria)
    {
        return response()->json([
            'status' => true,
            'data' => $categoria
        ]);
    }

    
    public function update(Request $request, Categoria $categoria)
    {
        $rules = ['categoria' => 'required|string|min:1|max:100'];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $categoria->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Categoria actualizada'
        ],200);
    }

    
    public function destroy(Categoria $categoria)
    {
        $categoria->delete();
        return response()->json([
            'status' => true,
            'message' => 'Categoria eliminada'
        ],200);
    }
}
