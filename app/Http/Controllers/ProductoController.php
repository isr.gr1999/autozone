<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Proveedor;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\returnSelf;

class ProductoController extends Controller
{
    
    public function index()
    {
        $producto = Producto::select('productos.*','categorias.categoria as categoria','proveedors.proveedor as proveedor')->join('categorias', 'productos.categoria_id', '=', 'categorias.id')
        ->join('proveedors', 'productos.proveedor_id', '=', 'proveedors.id')->paginate(10);
        return response()->json($producto);
    }

    
    public function store(Request $request)
    {
        $rules = [
            'nomProducto' => 'required|string|min:1|max:100',
            'categoria_id' => 'required|numeric',
            'proveedor_id' => 'required|numeric',
            'precio'  => 'required|string|min:1|max:50'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $producto = new Producto($request->input());
        $producto->save();
        return response()->json([
            'status' => true,
            'message' => 'Producto creado'
        ],200);
    }

    
    public function show(Producto $producto)
    {
        return response()->json([
            'status' => true,
            'data' => $producto
        ],200);
    }

    
    public function update(Request $request, Producto $producto)
    {
        $rules = [
            'nomProducto' => 'required|string|min:1|max:100',
            'categoria_id' => 'required|numeric',
            'proveedor_id' => 'required|numeric',
            'precio'  => 'required|string|min:1|max:50'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $producto->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Producto actualizado'
        ],200);
    }

    
    public function destroy(Producto $producto)
    {
        $producto->delete();
        return response()->json([
            'status' => true,
            'message' => 'Producto elimiando'
        ],200);
    }

    public function ProductoByCategoria(){
        $producto = Producto::select(DB::raw('count(productos.id) as count, categorias.categoria'))->join('categorias','categorias.id','=','productos.categoria_id')->groupBy('categorias.categoria')->get();
        return response()->json($producto);
    }

    public function ProductoByProveedor(){
        $producto = Producto::select(DB::raw('count(productos.id) as count ,proveedors.proveedor'))->join('proveedors','proveedors.id','=','productos.proveedor_id')->groupBy('proveedors.proveedor')->get();
        return response()->json($producto);
    }

    public function all(){
        $producto = Producto::select('productos.*','categorias.categoria as categoria','proveedors.proveedor as proveedor')->join('categorias', 'productos.categoria_id', '=', 'categorias.id')
        ->join('proveedors', 'productos.proveedor_id', '=', 'proveedors.id');
        return response()->json($producto);
    }
}
