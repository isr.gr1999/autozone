<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pedido>
 */
class PedidoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nomCliente' => $this->faker->name,
            'direccion' => $this->faker->word,
            'telefono' => $this->faker->e164PhoneNumber,
            'producto_id' => $this->faker->numberBetween(1,6)
        ];
    }
}
